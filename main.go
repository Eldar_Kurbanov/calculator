package main

import (
	"strconv"
)

// + - * /
// целые числа

type NumberBorder struct {
	leftBorder int
	rightBorder int
	number int
}

func isNumberToken(str uint8) bool {
	return str == '0' || str == '1' || str == '2' || str == '3' || str == '4' || str == '5' || str == '6' || str == '7' || str == '8' || str == '9'
}

func findNumbersNearbyOperand(str string, operandIndex int) (leftNumber NumberBorder, rightNumber NumberBorder) {
	var err error
	// find left number
	leftNumber.rightBorder = operandIndex - 1
	for i := operandIndex - 1; i >= 0 && isNumberToken(str[i]); i-- {
		leftNumber.leftBorder = i
	}
	leftNumber.number, err = strconv.Atoi(str[leftNumber.leftBorder : leftNumber.rightBorder + 1])
	if err != nil {
		panic("Err")
	}

	rightNumber.leftBorder = operandIndex + 1
	for i := operandIndex + 1; i < len(str) && isNumberToken(str[i]); i++ {
		rightNumber.rightBorder = i
	}
	rightNumber.number, err = strconv.Atoi(str[rightNumber.leftBorder : rightNumber.rightBorder + 1])
	if err != nil {
		panic("Err")
	}
	return
}

func replaceExpressionToResult(str string, resultExpression int, leftBorder int, rightBorder int) string {
	result := str[:leftBorder]
	resultExpressionStr := strconv.Itoa(resultExpression)
	result = result + resultExpressionStr
	result = result + str[rightBorder + 1:]
	return result
}

func multiplyLevel(str string, begin int, end int)  {
	multiples := make([]int, 0)
	for i, r := range str {
		c := string(r)
		if c == "*" || c == "/" {
			multiples = append(multiples, i)
		}
	}
	for _, operationIndex := range multiples {
		leftNumber, rightNumber := findNumbersNearbyOperand(str, operationIndex)
		var result = 0
		if str[operationIndex] == '*' {
			result = leftNumber.number * rightNumber.number
		}
		if str[operationIndex] == '/' {
			result = leftNumber.number / rightNumber.number
		}
		str = replaceExpressionToResult(str, result, leftNumber.leftBorder, rightNumber.rightBorder)
	}
}

func main()  {

}
