package main

import "testing"

func Test_multiplyLevel(t *testing.T) {
	str := "2+22*11"
	multiplyLevel(str, 0, len(str))
}

func Test_replaceExpressionToResult (t *testing.T) {
	str := "2+22/11"
	result := replaceExpressionToResult(str, 2, 2, 6)
	if result != "2+2" {
		t.Fatal()
	}
}

func Test_isNumberToken(t *testing.T) {
	result := isNumberToken('1')
	if !result {
		t.Fatal()
	}
	result = isNumberToken('*')
	if result {
		t.Fatal()
	}
}

func Test_findNumbersNearbyOperand(t *testing.T) {
	str := "2+5*1"
	leftNumber, rightNumber := findNumbersNearbyOperand(str, 3)
	if leftNumber.number != 5 || leftNumber.leftBorder != 2 || leftNumber.rightBorder != 2 {
		t.Fatal()
	}
	if rightNumber.number != 1 || rightNumber.leftBorder != 4 || rightNumber.rightBorder != 4 {
		t.Fatal()
	}
	str = "2+685*10"
	leftNumber, rightNumber = findNumbersNearbyOperand(str, 5)
	if leftNumber.number != 685 || leftNumber.leftBorder != 2 || leftNumber.rightBorder != 4 {
		t.Fatal()
	}
	if rightNumber.number != 10 || rightNumber.leftBorder != 6 || rightNumber.rightBorder != 7 {
		t.Fatal(rightNumber, rightNumber.leftBorder, rightNumber.rightBorder)
	}
}
